//
//  FlickController.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 16/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import UIKit

class EmptyView: UIView,NibLoadableView {
    @IBOutlet weak var noDataLabel: UILabel!
}
