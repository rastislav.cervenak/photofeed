//
//  TabBarViewController.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 15/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        itemsInit()
    }

    func itemsInit() {
        let tabBarItems: [TabBarItem] = [.tags, .feed]

        let controllers = tabBarItems.map { (item) -> UIViewController in
            let tabBarItem = item.controller()
            tabBarItem.tabBarItem = UITabBarItem(title: item.title(), image: item.image(), selectedImage: item.selectedImage())
            return tabBarItem
        }
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.blue], for: .selected)
        self.tabBar.tintColor = UIColor.yellow
        self.viewControllers = controllers
    }


}
