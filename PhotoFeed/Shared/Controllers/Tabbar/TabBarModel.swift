
//
//  TabBarModel.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 15/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import Foundation
import UIKit

enum TabBarItem: Int {
    case tags = 0
    case feed
}

extension TabBarItem {
    func title() -> String {
        var title = ""
        switch self {
        case .tags:
            title = NSLocalizedString("Tags", comment: "")
        case .feed:
            title = NSLocalizedString("Feed", comment: "")
        }
        return title
    }

    func image() -> UIImage? {
        var imageName: String?
        switch self {
        case .tags:
            imageName = ""
        case .feed:
            imageName = ""
        }
        if let imageName = imageName {
            return UIImage(named: imageName)?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        }
        return nil
    }

    func selectedImage() -> UIImage? {
        var imageName: String?
        switch self {
        case .tags:
            imageName = ""
        case .feed:
            imageName = ""
        }
        if let imageName = imageName {
            return UIImage(named: imageName)
        }
        return nil
    }

    func controller() -> UIViewController {
        switch self {
        case .tags:
            return TagsCollectionViewController()
        case .feed:
            return FeedOverviewTableViewController()
        }
    }
}
