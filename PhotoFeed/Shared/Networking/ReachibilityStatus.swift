//
//  ReachibilityStatus.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 16/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import Foundation
import Reachability

extension Notification.Name {
    static let isReachable = Notification.Name("isReachable")
    static let isNotReachable = Notification.Name("isNotReachable")
}

class ReachabilityStatus {

    init() {
        addObserverForNotification()
    }

    static let shared = ReachabilityStatus()
    let reachability = Reachability()!

    func isConnected() -> Bool {
        if reachability.connection == .none {
            return false
        } else {
            return true
        }
    }

    func addObserverForNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged(note:)),name: Notification.Name.reachabilityChanged, object: reachability)
        do {
            try reachability.startNotifier()
        } catch {
            print("could not start reachability notifier")
        }
    }

    @objc func reachabilityChanged(note: NSNotification) {
        if isConnected() {
            NotificationCenter.default.post(name: .isReachable, object: nil)
        } else {
            NotificationCenter.default.post(name: .isNotReachable, object: nil)
        }
    }
}
