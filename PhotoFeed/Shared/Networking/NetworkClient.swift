//
//  NetworkClient.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 16/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import UIKit

public final class NetworkClient {
    internal let baseUrl: URL

    public static let shared: NetworkClient = {
        return NetworkClient(baseURL: URL(string: "https://api.flickr.com/services/rest/")!)
    }()

    private init(baseURL: URL) {
        self.baseUrl = baseURL
    }

    let apiKey = "ea7917c50d84cc430e0ce4596e3734e4"

    let reachablityStatus = ReachabilityStatus.shared

    fileprivate let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.requestCachePolicy = .reloadIgnoringCacheData
        return Alamofire.SessionManager(configuration: configuration)
    }()

    func searchPhotosForTerm(_ searchTerm: String, completion: @escaping(Any?, NetworkError?) -> Void) {
        guard let url = flickrSearchURLForSearchTerm(searchTerm) else {
            completion(nil, NetworkError.badRequest)
            return
        }
        sessionManager.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (responseData) in
            guard let result = responseData.result.value else {
                if !self.reachablityStatus.isConnected() {
                    completion(nil, NetworkError.networkProblem)
                } else if let response = responseData.response {
                    let networkError = NetworkError(response: response)
                    completion(nil, networkError)
                } else {
                    completion(nil, NetworkError.serverDown)
                }
                return
            }

            completion(result, nil)
        }
    }

    
    public func downloadImage(imageURL: URL, imageView: UIImageView, placeholderImage: UIImage?) {
        imageView.af_setImage(withURL: imageURL, placeholderImage: placeholderImage)
    }


    fileprivate func flickrSearchURLForSearchTerm(_ searchTerm:String) -> URL? {

        guard let escapedTerm = searchTerm.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) else {
            return nil
        }

        let URLString = "?method=flickr.photos.search&api_key=\(apiKey)&text=\(escapedTerm)&per_page=3&format=json&nojsoncallback=1"

        guard let url = URL(string:"\(baseUrl)\(URLString)") else {
            return nil
        }

        return url
    }
}

