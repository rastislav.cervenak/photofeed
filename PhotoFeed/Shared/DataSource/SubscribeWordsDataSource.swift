//
//  SubscribeWordsDataSource.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 14/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import Foundation

protocol SubscribeWordsDataSourceDelegate {
    func updateWords()
}

class SubscribeWordsDataSource {
    static let shared = SubscribeWordsDataSource()
    var observers = [SubscribeWordsDataSourceDelegate]()
    
    var words: [Word] = []

    init() {
        setWords()
    }

    func addObserver(_ observer:  SubscribeWordsDataSourceDelegate) {
        self.observers.append(observer)
    }

    func setWords() {
        let rawWords = getWords()
        for rawWord in rawWords {
            let word = Word(title: rawWord, isSubscribed: false)
            words.append(word)
        }
    }

    func switchSubscriptionIndex(_ index: Int) {
        let isSubsribed = words[index].isSubscribed
        words[index].isSubscribed = !isSubsribed
        informObserversForNewWords()
    }

    func getSubscribedWords() -> [Word] {
        let subscribedWords = words.filter {
            $0.isSubscribed
        }
        return subscribedWords
    }

    private func getWords() -> [String] {
        let path = Bundle.main.path(forResource: "Words", ofType: "plist")!
        let items = NSArray(contentsOfFile: path) as! [String]
        return items
    }

    private func informObserversForNewWords() {
        for observer in observers {
            observer.updateWords()
        }
    }
}
