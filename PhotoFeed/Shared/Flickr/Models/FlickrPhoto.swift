//
//  FlickrPhoto.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 16/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import UIKit

enum ImageSize {
    case small
    case big

    func getSize() -> String {
        switch self {
        case .small:
            return "s"
        case .big:
            return "b"
        }
    }
}

class FlickrPhoto {
    var thumbnail : UIImage?
    var largeImage : UIImage?
    let photoID : String
    let farm : Int
    let server : String
    let secret : String

    init (photoID:String,farm:Int, server:String, secret:String) {
        self.photoID = photoID
        self.farm = farm
        self.server = server
        self.secret = secret
    }

    func flickrImageURL(_ size:String = "m") -> URL? {
        if let url =  URL(string: "https://farm\(farm).staticflickr.com/\(server)/\(photoID)_\(secret)_\(size).jpg") {
            return url
        }
        return nil
    }
}
