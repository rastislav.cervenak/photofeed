//
//  FlirckResult.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 16/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import Foundation

struct FlickrSearchResults {
    let searchTerm : String
    let searchResults : [FlickrPhoto]
}
