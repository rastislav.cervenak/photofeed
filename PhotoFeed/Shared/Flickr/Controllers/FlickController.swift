//
//  FlickController.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 16/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import UIKit

class FlickrController {
    static let shared = FlickrController()
    let networkClient = NetworkClient.shared

    public func getFlickerPhotosFor(words: [Word], completion:@escaping([FlickrSearchResults]?) -> Void) {
        var flickSearchResults = [FlickrSearchResults]()
        var downloadedItems = 0
        for word in words {
            self.getFlickerPhotosFor(word: word) { (flickrSearchresult) in
                if let flickrSearchresult = flickrSearchresult {
                    flickSearchResults.append(flickrSearchresult)
                }
                downloadedItems += 1
                if downloadedItems == words.count {
                    completion(flickSearchResults)
                }
            }
        }
    }

    public func downloadThumbnailForFlickerPhoto(_ flickerPhoto: FlickrPhoto, imageSize: ImageSize, in imageView: UIImageView) {
        guard let flickerImageUrl = flickerPhoto.flickrImageURL(imageSize.getSize()) else { return }
        networkClient.downloadImage(imageURL: flickerImageUrl, imageView: imageView, placeholderImage: UIImage(named: "noImage"))
    }

    private func getFlickerPhotosFor(word: Word, completion:@escaping(FlickrSearchResults?) -> Void) {

        guard let title = word.title else {
            completion(nil)
            return
        }
        networkClient.searchPhotosForTerm(title) { (data, error) in
            guard let data = data,
                let flickrPhotos = self.parseResponse(response: data) else {
                    completion(nil)
                    return
            }

            let flickrSearchResult = FlickrSearchResults(searchTerm: title, searchResults: flickrPhotos)
            completion(flickrSearchResult)
        }
    }

    private func parseResponse(response: Any) -> [FlickrPhoto]? {
        guard let response = response as? [String: Any],
            let photos = response["photos"] as? [String:Any],
            let photoData = photos["photo"] as? [[String:Any]] else {
                return nil
        }

        var flickrPhotos = [FlickrPhoto]()
        for photo in photoData {
            if let photoId = photo["id"] as? String,
                let farm = photo["farm"] as? Int,
                let server = photo["server"] as? String,
                let secret = photo["secret"] as? String {
                let flickrPhoto = FlickrPhoto(photoID: photoId, farm: farm, server: server, secret: secret)
                flickrPhotos.append(flickrPhoto)
            }
        }
        return flickrPhotos
    }
}
