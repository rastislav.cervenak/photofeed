//
//  TagCollectionViewCell.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 14/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import UIKit

protocol ReusableView: class {}

// MARK: -

extension ReusableView where Self: UIView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
