//
//  FeedOverviewTableViewController.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 15/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import UIKit

class FeedOverviewTableViewController: UITableViewController {
    let dataSource = FeedOverviewDataSource()
    var noDataView: EmptyView = {
        let view = EmptyView.loadFromXib() as! EmptyView
        return view
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(cellType: TagPhotosOverviewTableViewCell.self)
        tableView.dataSource = dataSource

        dataSource.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if dataSource.subcribedWords.count == 0 {
            noDataView.noDataLabel.text = "Please add at least one word"
            tableView.backgroundView = noDataView
        } else {
            tableView.backgroundView = nil
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        noDataView.frame = tableView.frame
    }
}

extension FeedOverviewTableViewController: FeedOverviewDataSourceDelegate {
    func reloadData() {
        tableView.reloadData()
    }
}
