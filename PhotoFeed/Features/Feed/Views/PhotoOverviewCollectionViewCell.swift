//
//  PhotoOverviewCollectionViewCell.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 15/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import UIKit

class PhotoOverviewCollectionViewCell: UICollectionViewCell, NibLoadableView {
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
}
