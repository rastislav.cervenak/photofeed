//
//  TagPhotosOverviewTableViewCell.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 15/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import UIKit

class TagPhotosOverviewTableViewCell: UITableViewCell, NibLoadableView {
    let dataSource = TagPhotosOverviewDataSource()
    let collectionViewLayout = UICollectionViewFlowLayout()

    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tagNameLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tagsOverviewCollectionView: UICollectionView! {
        didSet {
            tagsOverviewCollectionView.collectionViewLayout = collectionViewLayout
            tagsOverviewCollectionView.dataSource = dataSource
            tagsOverviewCollectionView.register(cellType: PhotoOverviewCollectionViewCell.self)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        tagsOverviewCollectionView.delegate = self
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        collectionViewHeightConstraint.constant = tagsOverviewCollectionView.frame.width / 3
        setLayout()
    }

    func setCellForWord(_ word: String) {
        tagNameLabel.text = word
    }

    func setCellForFlickrSearchResult(_ flickrSearchResult: FlickrSearchResults) {
        dataSource.flickrSearchResult = flickrSearchResult
        tagsOverviewCollectionView.reloadData()
    }


    private func setLayout() {
        collectionViewLayout.scrollDirection = .horizontal
        collectionViewLayout.minimumLineSpacing = 0
        collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

    }
}

extension TagPhotosOverviewTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = collectionView.frame.width
        let cellSize = width / 3
        return CGSize(width: cellSize, height: cellSize)
    }
}
