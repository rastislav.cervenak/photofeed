//
//  TagPhotosOverviewDataSource.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 15/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import UIKit

class TagPhotosOverviewDataSource: NSObject {
    let flickrController = FlickrController.shared
    var flickrSearchResult: FlickrSearchResults?
}

extension TagPhotosOverviewDataSource: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return flickrSearchResult?.searchResults.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as PhotoOverviewCollectionViewCell
        if let flickrSearchResult = flickrSearchResult {
            let flickrImage = flickrSearchResult.searchResults[indexPath.row]
            flickrController.downloadThumbnailForFlickerPhoto(flickrImage, imageSize: .small, in: cell.thumbnailImageView)
        }
        return cell
    }
}
