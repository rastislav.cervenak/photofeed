//
//  FeedOverviewDataSource.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 15/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import UIKit

protocol FeedOverviewDataSourceDelegate {
    func reloadData()
}

class FeedOverviewDataSource: NSObject {
    let subscribeWordsDataSource = SubscribeWordsDataSource.shared
    let flickrController = FlickrController.shared
    var delegate: FeedOverviewDataSourceDelegate?

    var subcribedWords = [Word]()
    var flickrSearchResults = [FlickrSearchResults]() {
        didSet {
            downloadPhotos()
        }
    }

    override init() {
        super.init()
        subscribeWordsDataSource.addObserver(self)
    }

    func setDataSource() {
        flickrSearchResults.removeAll()
        setSubscribedWords()
        getOnlineData()
        delegate?.reloadData()
    }

    private func setSubscribedWords() {
        subcribedWords = subscribeWordsDataSource.getSubscribedWords()
    }

    private func getOnlineData() {
        flickrController.getFlickerPhotosFor(words: subcribedWords) { (flickrSearchResults) in
            if let flickrSearchResults = flickrSearchResults {
                self.flickrSearchResults = flickrSearchResults
            }
        }
    }

    private func downloadPhotos() {
        delegate?.reloadData()
    }

}

extension FeedOverviewDataSource: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return flickrSearchResults.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as TagPhotosOverviewTableViewCell
        let flickSearch = flickrSearchResults[indexPath.row]
        cell.setCellForFlickrSearchResult(flickSearch)
        cell.setCellForWord(flickSearch.searchTerm)

        return cell
    }
}

extension FeedOverviewDataSource: SubscribeWordsDataSourceDelegate {
    func updateWords() {
        setDataSource()
    }
}
