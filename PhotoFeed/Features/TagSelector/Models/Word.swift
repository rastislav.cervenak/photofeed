//
//  Word.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 14/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import Foundation

struct Word {
    var title: String?
    var isSubscribed: Bool = false
}

