//
//  TagsCollectionViewControllerDataSource.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 14/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import UIKit

class TagsCollectionViewControllerDataSource: NSObject, UICollectionViewDataSource {
    let subscribedWordsDataSource = SubscribeWordsDataSource.shared
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subscribedWordsDataSource.words.count
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as TagCollectionViewCell
        let word = subscribedWordsDataSource.words[indexPath.row]
        cell.setCell(word: word)
        return cell
    }
}
