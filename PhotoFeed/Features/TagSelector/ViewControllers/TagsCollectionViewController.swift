//
//  TagsCollectionViewController.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 14/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import UIKit


class TagsCollectionViewController: UICollectionViewController {
    let dataSource = TagsCollectionViewControllerDataSource()

    init() {
        let layout = UICollectionViewFlowLayout()
        layout.estimatedItemSize = CGSize(width: 100, height: 100)
        layout.minimumInteritemSpacing = 2
        layout.minimumLineSpacing = 8
        layout.sectionInset = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
        super.init(collectionViewLayout: layout)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.register(cellType: TagCollectionViewCell.self)
        collectionView?.dataSource = dataSource
        collectionView?.backgroundColor = .white
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        dataSource.subscribedWordsDataSource.switchSubscriptionIndex(indexPath.row)
        collectionView.reloadData()
    }
}
