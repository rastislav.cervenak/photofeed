//
//  TagCollectionViewCell.swift
//  PhotoFeed
//
//  Created by Rastislav Cervenak on 14/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import UIKit

class TagCollectionViewCell: UICollectionViewCell, NibLoadableView {
    @IBOutlet weak var cellBackgroundView: UIView! {
        didSet {
            cellBackgroundView.layer.cornerRadius = 8
            cellBackgroundView.layer.borderWidth = 1
            setSubscibeView(isSubscribed: false)
        }
    }
    @IBOutlet weak var tagLabel: UILabel! {
        didSet {
            tagLabel.textColor = .black
        }
    }

    func setCell(word: Word) {
        setSubscibeView(isSubscribed: word.isSubscribed)
        tagLabel.text = word.title
    }

    func setSubscibeView(isSubscribed: Bool) {
        switch isSubscribed {
        case true:
            cellBackgroundView.backgroundColor = .gray
            cellBackgroundView.layer.borderColor = UIColor.clear.cgColor
        case false:
            cellBackgroundView.layer.borderColor = UIColor.black.cgColor
            cellBackgroundView.backgroundColor = .clear
        }
    }
}
