//
//  SubscribedWordsDataSourceTest.swift
//  PhotoFeedTests
//
//  Created by Rastislav Cervenak on 16/08/2018.
//  Copyright © 2018 Rastislav. All rights reserved.
//

import XCTest
@testable import PhotoFeed

class SubscribedWordsDataSourceTest: XCTestCase {

    func testSwitchWord() {
        let subscribeWordsDataSource = SubscribeWordsDataSource()
        let word = Word(title: "", isSubscribed: true)
        subscribeWordsDataSource.words.append(word)

        subscribeWordsDataSource.switchSubscriptionIndex(0)
        XCTAssertTrue(word.isSubscribed, "Switch subscription falsed")
    }

    func testGetSubscribedWords() {
        let subscribeWordsDataSource = SubscribeWordsDataSource()
        let word1 = Word(title: "", isSubscribed: true)
        let word2 = Word(title: "", isSubscribed: false)
        let word3 = Word(title: "", isSubscribed: true)

        subscribeWordsDataSource.words.append(contentsOf: [word1, word2, word3])
        let subscribedWords = subscribeWordsDataSource.getSubscribedWords()
        XCTAssertEqual(subscribedWords.count, 2, "Subscribed words return wrong count")

    }
    
}
